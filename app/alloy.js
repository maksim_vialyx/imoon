// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

var sound = null;

function playSound(name) {
	if (Ti.App.Properties.getBool('isSoundOn') == false) {
		return;
	};
	sound = Titanium.Media.createSound({
		url : '/sounds/' + name + '.mp3'
	});
	sound.play();
}

function L(text) {  
  if (Ti.App.languageXML === undefined || Ti.App.languageXML === null) {
    var langFile = Ti.App.Properties.getString('SETTING_LANGUAGE'); // We should store user's language setting in SETTING_LANGUAGE
    var file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'languages/' + langFile + '/strings.xml'); // Get the corresponding file from i18n
    if (!file.exists()) {
      var langFile = "en-EN"; // Fall back to english as the default language
      file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'languages/' + langFile + '/strings.xml');
    }
    var xmltext = file.read().text;
    var xmldata = Titanium.XML.parseString(xmltext); // Parse the xml
    Ti.App.languageXML = xmldata; // Store the parsed xml so that we don't parse everytime L() is called
  }
  // Get the localised string from xml file
  var xpath = "/resources/string[@name='" + text + "']/text()"; 
  var result = Ti.App.languageXML.evaluate(xpath).item(0);
  if (result) {
    return result.text;
  } else {
    return text; // Return the text if localised version not found
  }
}
