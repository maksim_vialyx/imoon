if (Ti.Platform.name == 'iPhone OS') {
	// $.overlayImage.hide();
	$.overlayImage.backgoundImage = $.overlayImage.image;
}

updateLanguage();
updateSoundBtn();

function close() {
	playSound('button_2');
	$.settings.close();
}

function openReviews() {
	playSound('button_3');
	Ti.Platform.openURL('https://www.weblancer.net/users/modisoftpro/portfolio/');	
}

function changeSound() {
	playSound('button_4');
	var isSoundOn = Ti.App.Properties.getBool('isSoundOn');
	Ti.App.Properties.setBool('isSoundOn', !isSoundOn);
	updateSoundBtn();
}

function changeLanguage() {
	playSound('button_5');
	var opts = {
  		cancel: 3,
  		options: ['Русский', 'English', 'Swedish', L('close')],
  		destructive: -1,
  		title: ''
	};
	
	var dialog = Ti.UI.createOptionDialog(opts);
	dialog.addEventListener('click', function(event) {
		if (event.index != -1) {
			//	TODO: set new locale
			var locale = 'en-EN';
			if (event.index == 0) {
				locale = 'ru-RU';
			} else if (event.index == 1) {
				locale = 'en-EN';
			} else if (event.index == 2) {
				locale = 'sv-SE';
			}
			
			Ti.App.languageXML = null;
			Ti.App.Properties.setString('SETTING_LANGUAGE', locale);
			updateLanguage();
		}
	});
	dialog.show();
}

function updateLanguage() {
	$.headerLabel.text = L('settings_title');
	$.descriptionTextView.value = L('settings_content');
	
	$.langLabel.text = L('settings_language');
	$.soundLabel.text = L('settings_sound');
	$.reviewsLabel.text = L('settings_reviews');
}

function updateSoundBtn() {
	var isSoundOn = Ti.App.Properties.getBool('isSoundOn');
	if (isSoundOn) {
		$.soundBtn.backgroundImage = "/images/sound_on.png";
	} else {
		$.soundBtn.backgroundImage = "/images/sound_off.png";
	};
}
