if (Ti.App.Properties.getObject('isSoundOn') == null) {
	Ti.App.Properties.setBool('isSoundOn', true);
}

if (Ti.App.Properties.getString('SETTING_LANGUAGE') == null) {
	Ti.App.Properties.setString('SETTING_LANGUAGE', Ti.Locale.currentLocale);
}

var videoPrefix = '1242_2208';
var videoSuffix = '';

var badDays = [9, 19, 29];
var neitralDays = [3, 5, 15, 23];
var moonDay = 0;

var settings = null;
var moonPlayer = null;
var interval = null;

var detailsBackground = null;

var settingsButton = null;
var dateView = null;
var dateLabel = null;
var dateTitleLabel = null;
var dateDescriptionLabel = null;

var pWidth;
var pHeight;

var isiOS = Titanium.Platform.name == 'iPhone OS';

var resumePlayer = function(e) {
	calculateMoonDay();
	moonPlayer.play();
	resumeSound();
};

var resumeSound = function() {
	if (settings == null) {
		stopMusicFunction();
		clearInterval(interval);
		interval = setInterval(function() {
			playMoonSound();
		}, 5 * 1000);
		playMoonSound();
	};
};

function playMoonSound() {
	if (detailsBackground == null) {
		playSound('moon_2');
	}
}

var stopMusicFunction = function() {
	clearInterval(interval);
	if (sound) {
		sound.stop();
	}
};

if (isiOS) {
	pWidth = Ti.Platform.displayCaps.platformWidth;
	pHeight = Ti.Platform.displayCaps.platformHeight;
	switch (pWidth) {
		case 320:
		if (pHeight == 480) {
			videoPrefix = '640_960';
		} else {
			videoPrefix = '750_1334';
		}
		break;
		case 375:
		videoPrefix = '750_1334';
		break;
		case 768:
		videoPrefix = '640_960';
		break;
		case 414:
		videoPrefix = '1242_2208';
		break;
		default:
		break;
	}
} else {
	videoPrefix = '640_960';
}

if (isiOS) {
	showMoon();
	dateView.opacity = 0;
	settingsButton.opacity = 0;
	moonPlayer.opacity = 0;
	showIntro();
}

Ti.API.info('videoPrefix: ' + videoPrefix);
Ti.API.info('width: ' + pWidth);
Ti.API.info('height: ' + pHeight);

$.index.open();

function showIntro() {
	var introPlayer = Titanium.Media.createVideoPlayer({
		url : '/video/' + videoPrefix + '_intro.mp4',    	
    	height : '100%',
    	width : '100%',
    	opacity : 0,
    	touchEnabled : false,
    	mediaControlStyle : Titanium.Media.VIDEO_CONTROL_NONE,
    	scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FILL,
    	repeatMode : Titanium.Media.VIDEO_REPEAT_MODE_ONE
	});
	
	var lastIntroImage = null;
	var view = Titanium.UI.createImageView({
  		opacity : 0,
  		width : Ti.UI.FILL,
  		height : Ti.UI.FILL,
  		backgroundColor : 'black'
	});
	var animation = Titanium.UI.createAnimation({
		opacity : 1,
		duration : 2 * 1000
	});
	var animationHandler = function() {
		playSound('moon_3');
		
  		introPlayer.opacity = 1;
  		view.opacity = 0;
  		view.image = lastIntroImage;
  		
  		setTimeout(function() {
  			var animation = Ti.UI.createAnimation({
    			opacity : 0,
    			duration : 2 * 1000
    		});
    		
			moonPlayer.opacity = 1;
    		
    		view.opacity = 1;
    		introPlayer.stop();
    		$.index.remove(introPlayer);
    		introPlayer = null;
    		
    		calculateMoonDay();    		
    		
    		moonPlayer.play();
    		
    		animation.addEventListener('complete', function() {
    			$.index.remove(view);
    			view = null;
    			dateView.opacity = 1;
				settingsButton.opacity = 1;
    			setTimeout(function() {
    				resumePlayer();
    			}, 3 * 1000);
    		});
    		
    		view.animate(animation);
  		}, 5 * 1000);
	};
	animation.addEventListener('complete', animationHandler);

	$.index.add(introPlayer);
	$.index.add(view);
	introPlayer.play();
	
	introPlayer.requestThumbnailImagesAtTimes([0, introPlayer.duration - 0.01], 0, function(event) {
		if (view.image == null) {
			view.image = event.image;
			view.animate(animation);
		} else {
			lastIntroImage = event.image;
		}
	});
}

function createDateView() {
	var size = Ti.Platform.displayCaps.platformWidth / 2;
	var viewSize = '200dp';
	dateView = Ti.UI.createView({
		backgroundColor : 'transparent',
		left : 0,
		top : '32sp',
		width : viewSize,
		height : viewSize
	});
	
	$.index.add(dateView);
	
	var image = Ti.UI.createImageView({
		image : '/images/dateImage.png'
	});
	
	dateView.add(image);
	
	var func = function() {
    		if (moonDay == 0) {
    			return;
    		};
    		detailsBackground = Ti.UI.createImageView({
    			image : "/images/background_settings.png",
    			width : Ti.UI.FILL,
    			height : Ti.UI.FILL
    		});
    		
    		$.index.add(detailsBackground);
    		
    		var dayLabel = Ti.UI.createLabel({
				color : 'white',
				height : "10%",
				top : "1%",
				left : "10sp",
				right : "10sp",
				font: {fontSize: '22sp', fontFamily:'HelveticaNeue-CondensedBold'},
  				textAlign : 'center'
    		});
    		
    		var currentTime = new Date();
			var month = currentTime.getMonth();
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
    		
    		var monthName = L('month_' + month + '_name');
    		dayLabel.text = day + ' ' + monthName + ' ' + year; 
    		
    		$.index.add(dayLabel);
    		
    		var textView = Ti.UI.createTextArea({
    			value : L('moonday_' + moonDay + '_description'),
    			backgroundColor : 'transparent',
    			editable : false,
    			color : 'white',
        		top : "12%",
        		left : "10sp",
        		right : "10sp",
        		bottom : "10sp",
        		font : {fontSize: '20sp'}
    		});
    		dateView.visible = false;
    		settingsButton.visible = false;
    		
    		$.index.add(textView);
    		stopMusicFunction();
    		
    		setTimeout(function() {
            	$.index.addEventListener("singletap", function() {
            		dateView.visible = true;
            		settingsButton.visible = true;
            		$.index.remove(dayLabel);
            		dayLabel = null;
    				$.index.remove(textView);
    				textView = null;
    				$.index.remove(detailsBackground);
    				detailsBackground = null;
    				$.index.removeEventListener('singletap', arguments.callee);
    				resumeSound();
    			});
          	}, 500);
    	};
	
	dateLabel = Ti.UI.createLabel({
		color : 'white',
		height : "20%",
		top : "20%",
		left : "15%",
		right : "20%",
		font: {fontSize: isiOS ? '38sp' : '32sp', fontFamily:'HelveticaNeue-CondensedBold'},
  		textAlign : 'center'
    });
    	
    dateView.add(dateLabel);
    
    dateTitleLabel = Ti.UI.createLabel({
		color : 'white',
		height : "10%",
		top : "40%",
		left : "15%",
		right : "20%",
		font: {fontSize: '16sp', fontFamily: 'HelveticaNeue-CondensedBold'},
  		textAlign : 'center'
    });
    	
    dateView.add(dateTitleLabel);
    
    dateDescriptionLabel = Ti.UI.createTextArea({
    	backgroundColor : 'transparent',
		color : 'white',
		height : "30%",
		top : "50%",
		left : "15%",	
		right : "20%",	
		font: {fontSize: '10sp'},
  		ellipsize : true,
  		textAlign : 'center',
  		editable : false
    });
    
    dateLabel.addEventListener('singletap', func);
    dateTitleLabel.addEventListener('singletap', func);
    dateDescriptionLabel.addEventListener('singletap', func);
    
    dateView.add(dateDescriptionLabel);
}

function showMoon() {
	if (moonPlayer == null) {
		moonPlayer = Titanium.Media.createVideoPlayer({
    		autoplay : false,
	    	height : '100%',
    		width : '100%',
    		touchEnabled : false,
	    	mediaControlStyle : Titanium.Media.VIDEO_CONTROL_NONE,
    		scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FILL,
    		repeatMode : Titanium.Media.VIDEO_REPEAT_MODE_ONE
		});
		
		if (Ti.Platform.name == 'android') {
			moonPlayer.addEventListener("playbackstate" , function (e) {
        		if (e.playbackState === Titanium.Media.VIDEO_PLAYBACK_STATE_STOPPED) {
            		moonPlayer.play();
        		}
    		});
		}
		
		$.index.add(moonPlayer);
		
		settingsButton = Ti.UI.createButton({
        	backgroundImage : '/images/settings.png',
        	height : "100sp",
        	width : "100sp",
        	bottom : 0,
        	right : 0,
        	opacity : 0
    	});
    	
    	settingsButton.addEventListener('click', function() {
    		stopMusicFunction();
    		
    		playSound('button_1');
    		settings = Alloy.createController('settings').getView();
    		
    		settings.addEventListener('blur', function() {
    			settings = null;
    			moonPlayer.play();
    		});
    		settings.addEventListener('close', function() {
    			resumePlayer();
    		});
    		clearInterval(interval);
    		
			settings.open();
    	});
    	
    	$.index.add(settingsButton);
    	
    	createDateView();
	} else {
		moonPlayer.autoPlay = true;
	}
	
	moonPlayer.url = '/video/' + videoPrefix + videoSuffix + '.mp4';
}

function calculateMoonDay() {
	var currentTime = new Date();
	currentTime.setHours(currentTime.getHours() + 5);
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	var hour = currentTime.getHours();
	var minute = currentTime.getMinutes();
	var moonYearValue = year - 2013;
	
	//	TODO: formula source - http://astronatal.ru/Articles/Moon/Moon.html
	
	sh_1=55; 
	sh_2=45; 
	dol_1=37; 
	dol_2=35;
	k1 = LD(day, month, year, hour, minute, sh_1, sh_2, dol_1, dol_2);
	
	if (badDays.indexOf(k1) > -1) {
		videoSuffix = '_active';
	} else {
		videoSuffix = '';
	}
	moonDay = k1;
	
	showMoon();
	
	var textColor = 'white';
	
	dateLabel.color = dateDescriptionLabel.color = dateTitleLabel.color = dateView.color = textColor;
	
	dateView.visible = moonDay > 0;
	dateLabel.text = moonDay;
	dateTitleLabel.text = L('moonday');
	dateDescriptionLabel.value = L('moonday_' + moonDay + '_title');
}

//	TODO: calculate lunar date
function moonzodiac()
{
var mnday=new Array(600);       mnz=new Array(600);


mnday[0]="03/02/2014  19:40";   mnz[0]="Овне";
mnday[1]="03/04/2014  23:12";   mnz[1]="Тельце";
mnday[2]="01/01/2015  19:08";   mnz[2]="Близнецах";
mnday[3]="01/04/2015  3:07";   mnz[3]="Раке";
mnday[4]="01/06/2015  13:02";   mnz[4]="Льве";
mnday[5]="01/09/2015  0:58";   mnz[5]="Деве";
mnday[6]="01/11/2015  13:56";   mnz[6]="Весах";
mnday[7]="01/14/2015  1:44";  mnz[7]="Скорпионе";
mnday[8]="01/16/2015  10:01";   mnz[8]="Стрельце";
mnday[9]="01/18/2015  14:04";   mnz[9]="Козероге";
mnday[10]="01/20/2015  14:59";   mnz[10]="Водолее";
mnday[11]="01/22/2015  14:48";   mnz[11]="Рыбах";

mnday[12]="01/24/2015  15:31";   mnz[12]="Овне";
mnday[13]="01/26/2015  18:37";   mnz[13]="Тельце";
mnday[14]="01/29/2015  0:36";   mnz[14]="Близнецах";
mnday[15]="01/31/2015  9:08";   mnz[15]="Раке";
mnday[16]="02/02/2015  19:41";   mnz[16]="Льве";
mnday[17]="02/05/2015  7:46";   mnz[17]="Деве";
mnday[18]="02/07/2015  20:43";   mnz[18]="Весах";
mnday[19]="02/10/2015  9:05";  mnz[19]="Скорпионе";
mnday[20]="02/12/2015  18:46";   mnz[20]="Стрельце";
mnday[21]="02/15/2015  0:24";   mnz[21]="Козероге";
mnday[22]="02/17/2015  2:13";   mnz[22]="Водолее";
mnday[23]="02/19/2015  1:47";   mnz[23]="Рыбах";

mnday[24]="02/21/2015  1:13";   mnz[24]="Овне";
mnday[25]="02/23/2015  2:28";   mnz[25]="Тельце";
mnday[26]="02/25/2015  6:54";   mnz[26]="Близнецах";
mnday[27]="02/27/2015  14:49";   mnz[27]="Раке";
mnday[28]="03/02/2015  1:34";   mnz[28]="Льве";
mnday[29]="03/04/2015  13:57";   mnz[29]="Деве";
mnday[30]="03/07/2015  2:52";   mnz[30]="Весах";
mnday[31]="03/09/2015  15:10";  mnz[31]="Скорпионе";
mnday[32]="03/12/2015  1:30";   mnz[32]="Стрельце";
mnday[33]="03/14/2015  8:40";   mnz[33]="Козероге";
mnday[34]="03/16/2015  12:14";   mnz[34]="Водолее";
mnday[35]="03/18/2015  12:58";   mnz[35]="Рыбах";

mnday[36]="03/20/2015  12:28";   mnz[36]="Овне";
mnday[37]="03/22/2015  12:40";   mnz[37]="Тельце";
mnday[38]="03/24/2015  15:22";   mnz[38]="Близнецах";
mnday[39]="03/26/2015  21:45";   mnz[39]="Раке";
mnday[40]="03/29/2015  7:48";   mnz[40]="Льве";
mnday[41]="03/31/2015  20:12";   mnz[41]="Деве";
mnday[42]="04/03/2015  9:07";   mnz[42]="Весах";
mnday[43]="04/05/2015  21:04";  mnz[43]="Скорпионе";
mnday[44]="04/08/2015  7:08";   mnz[44]="Стрельце";
mnday[45]="04/10/2015  14:47";   mnz[45]="Козероге";
mnday[46]="04/12/2015  19:44";   mnz[46]="Водолее";
mnday[47]="04/14/2015  22:12";   mnz[47]="Рыбах";

mnday[48]="04/16/2015  23:00";   mnz[48]="Овне";
mnday[49]="04/18/2015  23:31";   mnz[49]="Тельце";
mnday[50]="04/21/2015  1:28";   mnz[50]="Близнецах";
mnday[51]="04/23/2015  6:25";   mnz[51]="Раке";
mnday[52]="04/25/2015  15:12";   mnz[52]="Льве";
mnday[53]="04/28/2015  3:07";   mnz[53]="Деве";
mnday[54]="04/30/2015  16:03";   mnz[54]="Весах";
mnday[55]="05/03/2015  3:47";  mnz[55]="Скорпионе";
mnday[56]="05/05/2015  13:12";   mnz[56]="Стрельце";
mnday[57]="05/07/2015  20:16";   mnz[57]="Козероге";
mnday[58]="05/10/2015  1:22";   mnz[58]="Водолее";
mnday[59]="05/12/2015  4:53";   mnz[59]="Рыбах";

mnday[60]="05/14/2015  7:13";   mnz[60]="Овне";
mnday[61]="05/16/2015  9:02";   mnz[61]="Тельце";
mnday[62]="05/18/2015  11:27";   mnz[62]="Близнецах";
mnday[63]="05/20/2015  15:55";   mnz[63]="Раке";
mnday[64]="05/22/2015  23:42";   mnz[64]="Льве";
mnday[65]="05/25/2015  10:51";   mnz[65]="Деве";
mnday[66]="05/27/2015  23:42";   mnz[66]="Весах";
mnday[67]="05/30/2015  11:34";  mnz[67]="Скорпионе";
mnday[68]="06/01/2015  20:39";   mnz[68]="Стрельце";
mnday[69]="06/04/2015  2:50";   mnz[69]="Козероге";
mnday[70]="06/06/2015  7:02";   mnz[70]="Водолее";
mnday[71]="06/08/2015  10:16";   mnz[71]="Рыбах";

mnday[72]="06/10/2015  13:14";   mnz[72]="Овне";
mnday[73]="06/12/2015  16:16";   mnz[73]="Тельце";
mnday[74]="06/14/2015  19:51";   mnz[74]="Близнецах";
mnday[75]="06/17/2015  0:51";   mnz[75]="Раке";
mnday[76]="06/19/2015  8:22";   mnz[76]="Льве";
mnday[77]="06/21/2015  18:59";   mnz[77]="Деве";
mnday[78]="06/24/2015  7:41";   mnz[78]="Весах";
mnday[79]="06/26/2015  19:57";  mnz[79]="Скорпионе";
mnday[80]="06/29/2015  5:21";   mnz[80]="Стрельце";
mnday[81]="07/01/2015  11:11";   mnz[81]="Козероге";
mnday[82]="07/03/2015  14:21";   mnz[82]="Водолее";
mnday[83]="07/05/2015  16:23";   mnz[83]="Рыбах";

mnday[84]="07/07/2015  18:37";   mnz[84]="Овне";
mnday[85]="07/09/2015  21:49";   mnz[85]="Тельце";
mnday[86]="07/12/2015  2:16";   mnz[86]="Близнецах";
mnday[87]="07/14/2015  8:14";   mnz[87]="Раке";
mnday[88]="07/16/2015  16:15";   mnz[88]="Льве";
mnday[89]="07/19/2015  2:47";   mnz[89]="Деве";
mnday[90]="07/21/2015  15:23";   mnz[90]="Весах";
mnday[91]="07/24/2015  4:07";  mnz[91]="Скорпионе";
mnday[92]="07/26/2015  14:24";   mnz[92]="Стрельце";
mnday[93]="07/28/2015  20:47";   mnz[93]="Козероге";
mnday[94]="07/30/2015  23:40";   mnz[94]="Водолее";
mnday[95]="08/02/2015  0:36";   mnz[95]="Рыбах";

mnday[96]="08/04/2015  1:24";   mnz[96]="Овне";
mnday[97]="08/06/2015  3:29";   mnz[97]="Тельце";
mnday[98]="08/08/2015  7:40";   mnz[98]="Близнецах";
mnday[99]="08/10/2015  14:08";   mnz[99]="Раке";
mnday[100]="08/12/2015  22:52";   mnz[100]="Льве";
mnday[101]="08/15/2015  9:45";   mnz[101]="Деве";
mnday[102]="08/17/2015  22:22";   mnz[102]="Весах";
mnday[103]="08/20/2015  11:24";  mnz[103]="Скорпионе";
mnday[104]="08/22/2015  22:41";   mnz[104]="Стрельце";
mnday[105]="08/25/2015  6:22";   mnz[105]="Козероге";
mnday[106]="08/27/2015  10:03";   mnz[106]="Водолее";
mnday[107]="08/29/2015  10:51";   mnz[107]="Рыбах";

mnday[108]="08/31/2015  10:33";   mnz[108]="Овне";
mnday[109]="09/02/2015  11:01";   mnz[109]="Тельце";
mnday[110]="09/04/2015  13:47";   mnz[110]="Близнецах";
mnday[111]="09/06/2015  19:39";   mnz[111]="Раке";
mnday[112]="09/09/2015  4:36";   mnz[112]="Льве";
mnday[113]="09/11/2015  15:55";   mnz[113]="Деве";
mnday[114]="09/14/2015  4:41";   mnz[114]="Весах";
mnday[115]="09/16/2015  17:42";  mnz[115]="Скорпионе";
mnday[116]="09/19/2015  5:31";   mnz[116]="Стрельце";
mnday[117]="09/21/2015  14:33";   mnz[117]="Козероге";
mnday[118]="09/23/2015  19:51";   mnz[118]="Водолее";
mnday[119]="09/25/2015  21:43";   mnz[119]="Рыбах";

mnday[120]="09/27/2015  21:29";   mnz[120]="Овне";
mnday[121]="09/29/2015  20:57";   mnz[121]="Тельце";
mnday[122]="10/01/2015  22:03";   mnz[122]="Близнецах";
mnday[123]="10/04/2015  2:22";   mnz[123]="Раке";
mnday[124]="10/06/2015  10:30";   mnz[124]="Льве";
mnday[125]="10/08/2015  21:50";   mnz[125]="Деве";
mnday[126]="10/11/2015  10:45";   mnz[126]="Весах";
mnday[127]="10/13/2015  23:38";  mnz[127]="Скорпионе";
mnday[128]="10/16/2015  11:18";   mnz[128]="Стрельце";
mnday[129]="10/18/2015  20:52";   mnz[129]="Козероге";
mnday[130]="10/21/2015  3:37";   mnz[130]="Водолее";
mnday[131]="10/23/2015  7:17";   mnz[131]="Рыбах";

mnday[132]="10/25/2015  8:21";   mnz[132]="Овне";
mnday[133]="10/27/2015  8:07";   mnz[133]="Тельце";
mnday[134]="10/29/2015  8:24";   mnz[134]="Близнецах";
mnday[135]="10/31/2015  11:09";   mnz[135]="Раке";
mnday[136]="11/02/2015  17:47";   mnz[136]="Льве";
mnday[137]="11/05/2015  4:22";   mnz[137]="Деве";
mnday[138]="11/07/2015  17:14";   mnz[138]="Весах";
mnday[139]="11/10/2015  6:02";  mnz[139]="Скорпионе";
mnday[140]="11/12/2015  17:14";   mnz[140]="Стрельце";
mnday[141]="11/15/2015  2:21";   mnz[141]="Козероге";
mnday[142]="11/17/2015  9:24";   mnz[142]="Водолее";
mnday[143]="11/19/2015  14:21";   mnz[143]="Рыбах";

mnday[144]="11/21/2015  17:12";   mnz[144]="Овне";
mnday[145]="11/23/2015  18:26";   mnz[145]="Тельце";
mnday[146]="11/25/2015  19:15";   mnz[146]="Близнецах";
mnday[147]="11/27/2015  21:26";   mnz[147]="Раке";
mnday[148]="11/30/2015  2:47";   mnz[148]="Льве";
mnday[149]="12/02/2015  12:09";   mnz[149]="Деве";
mnday[150]="12/05/2015  0:33";   mnz[150]="Весах";
mnday[151]="12/07/2015  13:25";  mnz[151]="Скорпионе";
mnday[152]="12/10/2015  0:25";   mnz[152]="Стрельце";
mnday[153]="12/12/2015  8:46";   mnz[153]="Козероге";
mnday[154]="12/14/2015  14:58";   mnz[154]="Водолее";
mnday[155]="12/16/2015  19:44";   mnz[155]="Рыбах";

mnday[156]="12/18/2015  23:26";   mnz[156]="Овне";
mnday[157]="12/21/2015  2:12";   mnz[157]="Тельце";
mnday[158]="12/23/2015  4:31";   mnz[158]="Близнецах";
mnday[159]="12/25/2015  7:26";   mnz[159]="Раке";
mnday[160]="12/27/2015  12:31";   mnz[160]="Льве";
mnday[161]="12/29/2015  20:58";   mnz[161]="Деве";



var tdf=new Date();
  var tdl=new Date(); 
  var td=new Date();  td=Date.parse(td.toUTCString());

for(i=0; i<400; i++) {
 tdf=Date.parse(mnday[i]);
 tdl=Date.parse(mnday[i+1]);
 if ((td>tdf)&&(td<tdl)) {var mnd=mnz[i];}
 }
return "Луна в "+mnd;
}

function KRUG(A)
{ var KRUG1;  
  if (Math.abs(A)>1000000000000000000) { 
     A=0;
  }
  if (A>=0) { 
     KRUG1=A-360*Math.floor(A/360);} 
  else {
     KRUG1=A+360*Math.floor(Math.abs(A)/360)+360;
  }
  return (KRUG1);
}

function SN(X)
{ var SN1;
  SN1=Math.sin(X/180*Math.PI); 
  return (SN1);
}

function CS(X)
{ 
  var CS1;
  CS1=Math.cos(X/180*Math.PI);
  return (CS1);
}

function ATN(X)
{
  var ATN1;
  ATN1=180/Math.PI*Math.atan(X);
  return (ATN1);
}

function TT(D,M,Y,H,MIN,CEK) 
{
  var Jd,TT1;
  Jd=Math.floor(365.2500000001*(Y-1))-Math.floor(0.000000001+(Y-1)/100)+Math.floor(0.000000001+(Y-1)/400);
  Jd+=(Math.floor(0.000000001+(4-Y % 4)/4)-Math.floor(0.000000001+(100-Y % 100)/100)+Math.floor(0.000000001+(400-Y % 400)/400))*((Math.abs(M-2.1))/(2*M-4.2)+0.5);
  Jd+=Math.floor(30.5*M-30+0.5*Math.floor((Math.abs(M-8.1)+3*M-24.3)/(4*M-32.4)+0.0000001))-(Math.abs(M-2.1))/(M-2.1)-2+D+H/24+MIN/1440+CEK/86400-693594.5;
  TT1=(Jd+1/1440)/36525;
  return (TT1);
}

function SOLS(T)
{
  var SOLK,L,M,E,C,A,B,CC,D,EE,G,SOLS1;
  G=180/Math.PI;
  L= 279.69668+36000.76892*T+0.0003025*T*T;
  M= 358.475833+35999.04975*T-0.00015*T*T-0.0000033*T*T*T;
  E= 0.01675104-0.0000418*T-0.000000126*T*T;
  A=153.22+22518.7541*T; B=216.57+45037.5082*T;
  CC=312.69+32964.3577*T;
  D=350.74+445267.1142*T-0.00144*T*T;
  EE=231.19+20.2*T;
  C=(1.91946-0.004789*T-0.000014*T*T)*Math.sin(M/G)+(0.020094-0.0001*T)*Math.sin(M*2/G)+0.000293*Math.sin(M*3/G);
  SOLK=L+C+0.00134*Math.cos(A/G)+0.00154*Math.cos(B/G)+0.002*Math.cos(CC/G)+0.00179*Math.sin(D/G)+0.00178*Math.sin(EE/G);
  SOLS1=KRUG(SOLK-0.00569-0.00479*Math.sin(259.18/G-1934.142*T/G));
  return (SOLS1);
}

function LUNS(T)
{  
  var LUNK,E,L,M,MM,D,F,DT,Z,G,LUNS1;
  G=180/Math.PI;
  if (T<0.22) { DT=21.3-109.09*(0.22-T);}
  if (T>=0.22) { DT=22+15*(T-0.22);}
  if (T>=0.52) { DT=30+40*(T-0.52);}
  if (T>=0.62) { DT=34+80*(T-0.62);}
  if (T>=0.72) { DT=34+100*(T-0.72);}
  if (T>=0.82) { DT=52+50*(T-0.82);}
  T=T+DT/3600/24/36525;
  L=270.434164+481267.8831*T-0.001133*T*T+0.000233*SN(512+20.2*T);
  M=358.475833+35999.0498*T-0.001778*SN(512+20.2*T);
  MM=296.104608+477198.8491*T+0.009192*T*T+0.000817*SN(512+20.2*T);
  D=350.737486+445267.1142*T-0.001436*T*T+0.002011*SN(512+20.2*T);
  Z= 259.183275-1934.142*T+0.002078*T*T;
  F=11.250889+483202.0251*T-0.003211*T*T-0.024691*SN(Z);
  E=1-0.002495*T-0.00000752*T*T;
  LUNK=L+6.288750*Math.sin(MM/G)+1.274018*Math.sin((2*D-MM)/G)+0.658309*Math.sin((2*D)/G)+0.213616*Math.sin((2*MM)/G);
  LUNK-=0.185596*E*Math.sin(M/G);
  LUNK-=0.114336*Math.sin(2*F/G)+0.058793*Math.sin((2*D-2*MM)/G)+0.057212*E*Math.sin((2*D-M-MM)/G);
  LUNK+=0.053320*Math.sin((2*D+MM)/G);
  LUNK+=0.045874*E*Math.sin((2*D-M)/G)+0.041024*E*Math.sin((MM-M)/G)-0.034718*Math.sin(D/G)-0.030465*E*Math.sin((M+MM)/G);
  LUNK+=0.015326*Math.sin((2*D-2*F)/G);
  LUNK-=0.012528*Math.sin((2*F+MM)/G)-0.010980*Math.sin((2*F-MM)/G)+0.010674*Math.sin((4*D-MM)/G);
  LUNK+=0.010034*Math.sin(3*MM/G)+0.008548*Math.sin((4*D-2*M)/G)-0.00791*E*Math.sin((M-MM+2*D)/G);
  LUNK-=0.006783*E*SN(2*D+M)+0.005162*SN(MM-D)+0.005*SN(M+D)*E+0.004049*SN(MM-M+2*D)*E;
  LUNK+=0.003996*SN(2*MM+2*D)+0.003862*SN(4*D)+0.003665*Math.sin(2*D-3*MM);
  LUNK+=0.002695*SN(2*MM-M)*E+0.002602*SN(MM-2*F-2*D)+0.002396*SN(2*D-M-2*MM)*E;
  LUNK-=0.002349*SN(MM+D)+0.002249*SN(2*D-2*M)*E*E-0.002125*SN(2*MM+M)*E;
  LUNK-=0.002079*SN(2*M)*E*E+0.002059*SN(2*D-MM-2*M)*E*E;
  LUNS1=KRUG(LUNK+0.005);
  return (LUNS1);
}

function LUNBS(T) 
{
  var E,L,M,MM,D,F,DT,Z,G,LUNBS1;
  G=180/Math.PI;
  M=358.475833+35999.0498*T-0.00015*T*T;
  Z=259.183275-1934.142*T+0.002078*T*T;
  MM=296.104608+477198.8491*T+0.009192*T*T;
  D=350.737486+445267.1142*T-0.001436*T*T;
  F=11.250889+483202.0251*T-0.003211*T*T;
  E=1-0.002495*T-0.00000752*T*T;
  LUNBS1=5.128189*SN(F)+0.280606*SN(MM+F)+0.277693*SN(MM-F)+0.173238*SN(2*D-F);
  LUNBS1+=0.055413*SN(2*D+F-MM)+0.046272*SN(2*D-F-MM)+0.032573*SN(2*D+F);
  LUNBS1+=0.017198*SN(2*MM+F)+0.009267*SN(2*D+MM-F)+0.008823*SN(2*MM-F);
  LUNBS1+=E*0.008247*SN(2*D-M-F)+0.004323*SN(2*D-F-2*MM)+0.0042*SN(2*D+F+MM);
  LUNBS1+=E*0.003372*SN(F-M-2*D)+E*0.002472*SN(2*D+F-M-MM)+E*0.002222*SN(2*D+F-M);
  LUNBS1+=E*0.002072*SN(2*D-F-M-MM);
  return (LUNBS1);
}

function RAMCT(T,LNG,LNM)
{
  var TTO,RO,TD,DT,RAMCT1;
  TD=T*36525-0.5-Math.floor(T*36525-0.5);
  if (TD<0) { TD=TD+1;}
  TTO=T-TD/36525;
  if (T<0.22)  { DT=21.3-109.09*(0.22-T);}
  if (T>=0.22) { DT=22+15*(T-0.22);}
  if (T>=0.52) { DT=30+40*(T-0.52);}
  if (T>=0.62) { DT=34+80*(T-0.62);}
  if (T>=0.72) { DT=34+100*(T-0.72);}
  if (T>=0.82) { DT=52+50*(T-0.82);}
  RO=6.6460656+2400.05162*TTO+0.00002581*TTO*TTO-(T-0.5)/1000;
  RAMCT1=KRUG((RO+TD*24+LNG/15+LNM/900+TD*0.065833+DT/3600)*15);
  return (RAMCT1);
}

function NV(L,B,T,N,BRG,BRM)
{
  var SD,D,SPV,PV,SRV,CPV,NVi,RV,BR,G,NV1,E;
  E=23.452294-0.0130125*T-0.00000164*T*T+0.000000503*T*T*T;
  BR=BRG+BRM/60;
  G=180/Math.PI; 
  if (Math.abs(BR)>(90-E))
  { 
     BRG=0;
     BRM=0;
  }
  BR=BRG+BRM/60;
  SD=CS(E)*SN(B)+SN(E)*CS(B)*SN(L);
  D=ATN(SD/Math.sqrt(1-SD*SD));
  if ((L>90)&&(L<270))
  { 
    SPV=(CS(E)*SN(D)-SN(B))/SN(E)/CS(D);
    PV=180-ATN(SPV/(Math.sqrt(Math.abs(1-SPV*SPV))));
    if ((PV<90)||(PV>270)) { PV=ATN(-SPV/(Math.sqrt(Math.abs(1-SPV*SPV))));}
  }
  if ((L<=90)||(L>=270))
  { 
    SPV=(CS(E)*SN(D)-SN(B))/SN(E)/CS(D);
    PV=ATN(SPV/(Math.sqrt(Math.abs(1-SPV*SPV))));
    if ((PV>=90)&&(PV<=270)) { PV=ATN(-SPV/(Math.sqrt(Math.abs(1-SPV*SPV))));}
  }
  SRV=SN(D)/CS(D)*SN(BR)/CS(BR);
  if (Math.abs(SRV)>=1) { NV1=-1; } else 
  { 
    RV=G*Math.atan(SRV/Math.sqrt(1-SRV*SRV));
    if (N<=7) {NVi=PV+RV*(N-4)/3;} 
    else {NVi=PV+RV*(10-N)/3;}
    NV1=KRUG(NVi);
  }
  return (NV1);
}

function VOSL(T,BRG,BRM,LNG,LNM )
{
  var RAMC,TL,ED1,ED2,VOSL1;
 
  RAMC=RAMCT(T,LNG,LNM);
  ED1=KRUG(NV(LUNS(T),LUNBS(T),T,1,BRG,BRM)-RAMC+270);
  TL=T+ED1/360/36525;
  ED2=KRUG(NV(LUNS(TL),LUNBS(TL),T,1,BRG,BRM)-RAMC+270);
  if ((ED1>300)&&(ED2<180))  {ED2=ED2+360;}
  if ((NV(LUNS(TL),LUNBS(TL),T,1,BRG,BRM)<0)||(Math.abs(BRG+BRM/60)>66.56)) {ED2=-1;}
  VOSL1=Math.floor(ED2*4)+2;
  return (VOSL1);
}

function OKRUG(A)
{ var OKRUG1;
  if (KRUG(A)>180) { OKRUG1=KRUG(A)-360;} else {OKRUG1=KRUG(A);}
  return (OKRUG1);
}

function NOL(T) 
{ var TN,T1,NOL1;

  TN=KRUG(LUNS(T)-SOLS(T));
  T1=T-TN/12.2/36525;
  TN=OKRUG(LUNS(T1)-SOLS(T1));
  T1=T1-TN/12.2/36525;
  TN=OKRUG(LUNS(T1)-SOLS(T1));
  T1=T1-TN/12.2/36525;
  TN=OKRUG(LUNS(T1)-SOLS(T1));
  T1=T1-TN/12.2/36525;
  TN=OKRUG(LUNS(T1)-SOLS(T1));
  NOL1=T1-TN/12.2/36525;
  return (NOL1);
}

function ZNS(D)
{ var ZNS1;
  switch (D)
  { 
 case 1:ZNS1='Oven';
        break;
 case 13:ZNS1='Oven';
        break;
 case 14:ZNS1='Telez';
        break;
 case 2:ZNS1='Telez';
        break;
 case 3:ZNS1='Bliznezi';
        break;
 case 15:ZNS1='Bliznezi';
        break;
 case 4:ZNS1='Pak';
        break;
 case 16:ZNS1='Pak';
        break;
 case 5:ZNS1='Lev';
        break;
 case 17:ZNS1='Lev';
        break;
 case 6:ZNS1='Deva';
        break;
 case 18:ZNS1='Deva';
        break;
 case 7:ZNS1='Vesi';
        break;
 case 8:ZNS1='Ckorpion';
        break;
 case 9:ZNS1='Ctrelez';
        break;
 case 10:ZNS1='Kozerog';
        break;
 case 11:ZNS1='Vodolej';
        break;
 case 12:ZNS1='Pibi';
        break;
  
  }
   return (ZNS1);
}

function LD(D,M,Y,H,MIN,BRG,BRM,LNG,LNM)
{
  var T,TN,TV,TVN,LD1;
  T=TT(D,M,Y,H,MIN,0);
  TV=T+Math.abs(T,BRG,BRM,LNG,LNM)/1440/36525;
  if (TV<T) {LD1=0;} 
  else 
  { 
    TN=NOL(T);
    TVN=TN+Math.abs(TN,BRG,BRM,LNG,LNM)/1440/36525;
    LD1=Math.round((TV-TVN)*35266)+1;
  }
  return (LD1);
}

function LZ(D,M,Y,H,MIN)
{ 
  var LZ1;
  LZ1=ZNS(1+Math.floor(LUNS(TT(D,M,Y,H,MIN,0))/30));
  return (LZ1);
}


function novi()
{
var vis,vis_,koef1,koef1_,koef,koef_,minut,minut1;
mas=new Array(10);
mas[0]=new Array(0,9.8,18.6,28.4,7.6,17.4,26.2,6.5,15.3,25.1);
mas[1]=new Array(18.9,28.7,8,17.7,26.5,8.8,15.6,25.4,4.6,14.4);
mas[2]=new Array(8.3,17.1,26.9,6.1,15.9,24.7,5,13.8,23.5,2.8);
mas[3]=new Array(27.2,6.4,16.2,25,5.3,14.1,23.9,3.1,12.9,21.7);
mas[4]=new Array(15.5,25.3,4.6,14.4,23.2,3.4,12.2,22,1.8,11.1);
mas[5]=new Array(4.9,14.7,23.5,3.7,12.5,22.3,1.6,11.4,20.2,0.5);
mas[6]=new Array(23.8,3.1,12.8,21.6,1.9,10.7,20.5,29.3,9.5,18.3);
mas[7]=new Array(13.2,22,2.2,11,20.8,0.1,9.9,18.7,28.4,7.7);
mas[8]=new Array(1.5,11.3,20.1,0.4,9.2,19,27.8,8,16.8,26.6);
mas[9]=new Array(20.4,0.7,9.5,19.3,28.1,8.3,17.1,26.9,6.2,16);

var segodnya=new Date();
var den=segodnya.getUTCDate();
var mec=segodnya.getUTCMonth();
var jear=segodnya.getUTCFullYear();

var mec1=mec+1;
var jear1=jear;

//if (mec1==13) {jear1=jear1+1; mec1=1}

var k=Math.floor(jear/100);
var k1=Math.floor(jear1/100);

switch (k)
{
	case 18:koef=1.5;
	break;
	case 19:koef=6.8;
	break;
	case 20:koef=11.2;
	break;
	case 21:koef=16.5;
		break;
}

switch (k1)
{
	case 18:koef_=1.5;
	break;
	case 19:koef_=6.8;
	break;
	case 20:koef_=11.2;
	break;
	case 21:koef_=16.5;
		break;
}

var d=Math.floor((jear-k*100)/10);
var d1=Math.floor((jear1-k1*100)/10);

var od=jear-k*100-d*10;
var od1=jear1-k1*100-d1*10;

if ((jear%400)==0) {vis=1;}
else 
{   if ((jear%4==0)&&(jear%100!=0)) {vis=1;}
        else {vis=0;} 
}

if ((jear1%400)==0) {vis_=1;}
else 
{   if ((jear1%4==0)&&(jear1%100!=0)) {vis_=1;}
        else {vis_=0;} 
}

switch (mec)
{
    case 1: koef1=24;
    break;
	case 2: koef1=22.5;
	break;
 	case 3: koef1=24.1;
	break;
	case 4: koef1=22.6;
	break;
	case 5: koef1=22.1;
	break;
	case 6: koef1=20.7;
	break;
	case 7: koef1=20.2;
	break;
	case 8: koef1=18.7;
	break;
	case 9: koef1=17.2;
	break;
	case 10:koef1=16.8;
	break;
	case 11:koef1=15.3;
	break;
	case 12:koef1=14.8;
	break;
}

switch (mec1)
{
    case 1: koef1_=24;
    break;
	case 2: koef1_=22.5;
	break;
 	case 3: koef1_=24.1;
	break;
	case 4: koef1_=22.6;
	break;
	case 5: koef1_=22.1;
	break;
	case 6: koef1_=20.7;
	break;
	case 7: koef1_=20.2;
	break;
	case 8: koef1_=18.7;
	break;
	case 9: koef1_=17.2;
	break;
	case 10:koef1_=16.8;
	break;
	case 11:koef1_=15.3;
	break;
	case 12:koef1_=14.8;
	break;
}

if ((mec<2)&&(vis==1))
{
   if (mec==1) {koef1=25;}
   else 
   {
    if (mec==2) {koef1=23.5;}
   }
}

if ((mec1<2)&&(vis_==1))
{
   if (mec1==1) {koef1_=25;}
   else 
   {
    if (mec1==2) {koef1_=23.5;}
   }
}

var zloj=koef+mas[od][d]+koef1;
var zloj1=koef_+mas[od1][d1]+koef1_;

while (zloj>29.5)
{zloj=zloj-29.5;}
while (zloj1>29.5)
{zloj1=zloj1-29.5;}

if (zloj<1) {zloj=zloj+29.5;}
if (zloj1<1) {zloj1=zloj1+29.5;}

var dej=Math.floor(zloj);
var dej1=Math.floor(zloj1);

var chas=zloj-dej;
var chas1=zloj1-dej1;

chas=chas*24;
chas1=chas1*24;

if ((chas-Math.floor(chas))>0) {
      minut=(chas-Math.floor(chas))*60;
      chas=Math.floor(chas);
}

if ((chas1-Math.floor(chas1))>0) {
      minut1=(chas1-Math.floor(chas1))*60;
      chas1=Math.floor(chas1);
}

chas=chas+2;
chas1=chas1+2;

//if (chas==24){dej++;chas=0}
//if (chas1==24){dej1++;chas1=0}
//if (chas==25){dej++;chas=1}
//if (chas1==25){dej1++;chas1=1}
//if (chas==26){dej++;chas=2}
//if (chas1==26){dej1++;chas1=2}

var mec_new=mec;
var mec_new1=mec1;
var jear_new=jear;
var jear_new1=jear1;

//if ((mec==2)&&(((vis==1)&&(dej==29))||((vis==0)&&(dej==28)))) {
	//	mec_new=3;dej=1;}
//if (((mec==1)||(mec==3)||(mec==5)||(mec==7)||(mec==8)||(mec==10))&&(dej==31)){
  //     mec_new++;dej=1;}
//if (((mec==4)||(mec==6)||(mec==9)||(mec==11))&&(dej==30)){
  //     mec_new++;dej=1;}
//if ((mec==12)&&(den==31)){
  //    jear_new++;mec_new=1;dej=1;}

//if ((mec1==2)&&(((vis_==1)&&(dej1==29))||((vis_==0)&&(dej1==28)))) {
	//   mec_new1=3;dej1=1;}
//if (((mec1==1)||(mec1==3)||(mec1==5)||(mec1==7)||(mec1==8)||(mec1==10))&&(dej1==31)){
  //     mec_new++;dej1=1;}
//if (((mec1==4)||(mec1==6)||(mec1==9)||(mec1==11))&&(dej1==30)){
  //     mec_new1++;dej1=1;}
//if ((mec1==12)&&(den1==31)){
  //     jear1_new++;mec_new1=1;dej1=1;}

var nov;

if (dej1>den){
 nov=LZ(dej,mec_new,jear_new,chas,Math.floor(minut));	   
}
if (dej1<=den){
 nov=LZ(dej1,mec_new1,jear_new1,chas1,Math.floor(minut1));	   
}
   	   
//var nov1=dej+":"+mec+' '+chas+":"+Math.floor(minut)

return (nov);
}
//

$.index.addEventListener('open', function() {
	showMoon();
	dateView.opacity = 0;
	settingsButton.opacity = 0;
	moonPlayer.opacity = 0;
	showIntro();
	
	if (isiOS == false) {
		$.index.activity.addEventListener('resume', resumePlayer);
		$.index.activity.addEventListener('pause', stopMusicFunction);
		$.index.activity.addEventListener('stop', function() {
			$.index.activity.removeEventListener('resume', resumePlayer);
		});
	} else {
		Ti.App.addEventListener('resume', resumePlayer);
		Ti.App.addEventListener('pause', stopMusicFunction);
	}
});
