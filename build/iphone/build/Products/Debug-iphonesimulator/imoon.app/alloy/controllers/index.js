function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function playMoonSound() {
        null == detailsBackground && playSound("moon_2");
    }
    function showIntro() {
        var introPlayer = Titanium.Media.createVideoPlayer({
            url: "/video/" + videoPrefix + "_intro.mp4",
            height: "100%",
            width: "100%",
            opacity: 0,
            touchEnabled: false,
            mediaControlStyle: Titanium.Media.VIDEO_CONTROL_NONE,
            scalingMode: Titanium.Media.VIDEO_SCALING_ASPECT_FILL,
            repeatMode: Titanium.Media.VIDEO_REPEAT_MODE_ONE
        });
        var lastIntroImage = null;
        var view = Titanium.UI.createImageView({
            opacity: 0,
            width: Ti.UI.FILL,
            height: Ti.UI.FILL,
            backgroundColor: "black"
        });
        var animation = Titanium.UI.createAnimation({
            opacity: 1,
            duration: 2e3
        });
        var animationHandler = function() {
            playSound("moon_3");
            introPlayer.opacity = 1;
            view.opacity = 0;
            view.image = lastIntroImage;
            setTimeout(function() {
                var animation = Ti.UI.createAnimation({
                    opacity: 0,
                    duration: 2e3
                });
                moonPlayer.opacity = 1;
                view.opacity = 1;
                introPlayer.stop();
                $.index.remove(introPlayer);
                introPlayer = null;
                calculateMoonDay();
                moonPlayer.play();
                animation.addEventListener("complete", function() {
                    $.index.remove(view);
                    view = null;
                    dateView.opacity = 1;
                    settingsButton.opacity = 1;
                    setTimeout(function() {
                        resumePlayer();
                    }, 3e3);
                });
                view.animate(animation);
            }, 5e3);
        };
        animation.addEventListener("complete", animationHandler);
        $.index.add(introPlayer);
        $.index.add(view);
        introPlayer.play();
        introPlayer.requestThumbnailImagesAtTimes([ 0, introPlayer.duration - .01 ], 0, function(event) {
            if (null == view.image) {
                view.image = event.image;
                view.animate(animation);
            } else lastIntroImage = event.image;
        });
    }
    function createDateView() {
        Ti.Platform.displayCaps.platformWidth / 2;
        var viewSize = "200dp";
        dateView = Ti.UI.createView({
            backgroundColor: "transparent",
            left: 0,
            top: "32sp",
            width: viewSize,
            height: viewSize
        });
        $.index.add(dateView);
        var image = Ti.UI.createImageView({
            image: "/images/dateImage.png"
        });
        dateView.add(image);
        var func = function() {
            if (0 == moonDay) return;
            detailsBackground = Ti.UI.createImageView({
                image: "/images/background_settings.png",
                width: Ti.UI.FILL,
                height: Ti.UI.FILL
            });
            $.index.add(detailsBackground);
            var dayLabel = Ti.UI.createLabel({
                color: "white",
                height: "10%",
                top: "1%",
                left: "10sp",
                right: "10sp",
                font: {
                    fontSize: "22sp",
                    fontFamily: "HelveticaNeue-CondensedBold"
                },
                textAlign: "center"
            });
            var currentTime = new Date();
            var month = currentTime.getMonth();
            var day = currentTime.getDate();
            var year = currentTime.getFullYear();
            dayLabel.text = day + " " + L("month_" + month + "_name") + " " + year;
            $.index.add(dayLabel);
            var textView = Ti.UI.createTextArea({
                value: L("moonday_" + moonDay + "_description"),
                backgroundColor: "transparent",
                editable: false,
                color: "white",
                top: "12%",
                left: "10sp",
                right: "10sp",
                bottom: "10sp",
                font: {
                    fontSize: "20sp"
                }
            });
            dateView.visible = false;
            settingsButton.visible = false;
            $.index.add(textView);
            stopMusicFunction();
            setTimeout(function() {
                $.index.addEventListener("singletap", function() {
                    dateView.visible = true;
                    settingsButton.visible = true;
                    $.index.remove(dayLabel);
                    dayLabel = null;
                    $.index.remove(textView);
                    textView = null;
                    $.index.remove(detailsBackground);
                    detailsBackground = null;
                    $.index.removeEventListener("singletap", arguments.callee);
                    resumeSound();
                });
            }, 500);
        };
        dateLabel = Ti.UI.createLabel({
            color: "white",
            height: "20%",
            top: "20%",
            left: "15%",
            right: "20%",
            font: {
                fontSize: isiOS ? "38sp" : "32sp",
                fontFamily: "HelveticaNeue-CondensedBold"
            },
            textAlign: "center"
        });
        dateView.add(dateLabel);
        dateTitleLabel = Ti.UI.createLabel({
            color: "white",
            height: "10%",
            top: "40%",
            left: "15%",
            right: "20%",
            font: {
                fontSize: "16sp",
                fontFamily: "HelveticaNeue-CondensedBold"
            },
            textAlign: "center"
        });
        dateView.add(dateTitleLabel);
        dateDescriptionLabel = Ti.UI.createTextArea({
            backgroundColor: "transparent",
            color: "white",
            height: "30%",
            top: "50%",
            left: "15%",
            right: "20%",
            font: {
                fontSize: "10sp"
            },
            ellipsize: true,
            textAlign: "center",
            editable: false
        });
        dateLabel.addEventListener("singletap", func);
        dateTitleLabel.addEventListener("singletap", func);
        dateDescriptionLabel.addEventListener("singletap", func);
        dateView.add(dateDescriptionLabel);
    }
    function showMoon() {
        if (null == moonPlayer) {
            moonPlayer = Titanium.Media.createVideoPlayer({
                autoplay: false,
                height: "100%",
                width: "100%",
                touchEnabled: false,
                mediaControlStyle: Titanium.Media.VIDEO_CONTROL_NONE,
                scalingMode: Titanium.Media.VIDEO_SCALING_ASPECT_FILL,
                repeatMode: Titanium.Media.VIDEO_REPEAT_MODE_ONE
            });
            $.index.add(moonPlayer);
            settingsButton = Ti.UI.createButton({
                backgroundImage: "/images/settings.png",
                height: "100sp",
                width: "100sp",
                bottom: 0,
                right: 0,
                opacity: 0
            });
            settingsButton.addEventListener("click", function() {
                stopMusicFunction();
                playSound("button_1");
                settings = Alloy.createController("settings").getView();
                settings.addEventListener("blur", function() {
                    settings = null;
                    moonPlayer.play();
                });
                settings.addEventListener("close", function() {
                    resumePlayer();
                });
                clearInterval(interval);
                settings.open();
            });
            $.index.add(settingsButton);
            createDateView();
        } else moonPlayer.autoPlay = true;
        moonPlayer.url = "/video/" + videoPrefix + videoSuffix + ".mp4";
    }
    function calculateMoonDay() {
        var currentTime = new Date();
        currentTime.setHours(currentTime.getHours() + 5);
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var hour = currentTime.getHours();
        var minute = currentTime.getMinutes();
        sh_1 = 55;
        sh_2 = 45;
        dol_1 = 37;
        dol_2 = 35;
        k1 = LD(day, month, year, hour, minute, sh_1, sh_2, dol_1, dol_2);
        videoSuffix = badDays.indexOf(k1) > -1 ? "_active" : "";
        moonDay = k1;
        showMoon();
        var textColor = "white";
        dateLabel.color = dateDescriptionLabel.color = dateTitleLabel.color = dateView.color = textColor;
        dateView.visible = moonDay > 0;
        dateLabel.text = moonDay;
        dateTitleLabel.text = L("moonday");
        dateDescriptionLabel.value = L("moonday_" + moonDay + "_title");
    }
    function KRUG(A) {
        var KRUG1;
        Math.abs(A) > 1e18 && (A = 0);
        KRUG1 = A >= 0 ? A - 360 * Math.floor(A / 360) : A + 360 * Math.floor(Math.abs(A) / 360) + 360;
        return KRUG1;
    }
    function SN(X) {
        var SN1;
        SN1 = Math.sin(X / 180 * Math.PI);
        return SN1;
    }
    function TT(D, M, Y, H, MIN, CEK) {
        var Jd, TT1;
        Jd = Math.floor(365.2500000001 * (Y - 1)) - Math.floor(1e-9 + (Y - 1) / 100) + Math.floor(1e-9 + (Y - 1) / 400);
        Jd += (Math.floor(1e-9 + (4 - Y % 4) / 4) - Math.floor(1e-9 + (100 - Y % 100) / 100) + Math.floor(1e-9 + (400 - Y % 400) / 400)) * (Math.abs(M - 2.1) / (2 * M - 4.2) + .5);
        Jd += Math.floor(30.5 * M - 30 + .5 * Math.floor((Math.abs(M - 8.1) + 3 * M - 24.3) / (4 * M - 32.4) + 1e-7)) - Math.abs(M - 2.1) / (M - 2.1) - 2 + D + H / 24 + MIN / 1440 + CEK / 86400 - 693594.5;
        TT1 = (Jd + 1 / 1440) / 36525;
        return TT1;
    }
    function SOLS(T) {
        var SOLK, L, M, E, C, A, B, CC, D, EE, G, SOLS1;
        G = 180 / Math.PI;
        L = 279.69668 + 36000.76892 * T + 3025e-7 * T * T;
        M = 358.475833 + 35999.04975 * T - 15e-5 * T * T - 33e-7 * T * T * T;
        E = .01675104 - 418e-7 * T - 1.26e-7 * T * T;
        A = 153.22 + 22518.7541 * T;
        B = 216.57 + 45037.5082 * T;
        CC = 312.69 + 32964.3577 * T;
        D = 350.74 + 445267.1142 * T - .00144 * T * T;
        EE = 231.19 + 20.2 * T;
        C = (1.91946 - .004789 * T - 14e-6 * T * T) * Math.sin(M / G) + (.020094 - 1e-4 * T) * Math.sin(2 * M / G) + 293e-6 * Math.sin(3 * M / G);
        SOLK = L + C + .00134 * Math.cos(A / G) + .00154 * Math.cos(B / G) + .002 * Math.cos(CC / G) + .00179 * Math.sin(D / G) + .00178 * Math.sin(EE / G);
        SOLS1 = KRUG(SOLK - .00569 - .00479 * Math.sin(259.18 / G - 1934.142 * T / G));
        return SOLS1;
    }
    function LUNS(T) {
        var LUNK, E, L, M, MM, D, F, DT, Z, G, LUNS1;
        G = 180 / Math.PI;
        .22 > T && (DT = 21.3 - 109.09 * (.22 - T));
        T >= .22 && (DT = 22 + 15 * (T - .22));
        T >= .52 && (DT = 30 + 40 * (T - .52));
        T >= .62 && (DT = 34 + 80 * (T - .62));
        T >= .72 && (DT = 34 + 100 * (T - .72));
        T >= .82 && (DT = 52 + 50 * (T - .82));
        T += DT / 3600 / 24 / 36525;
        L = 270.434164 + 481267.8831 * T - .001133 * T * T + 233e-6 * SN(512 + 20.2 * T);
        M = 358.475833 + 35999.0498 * T - .001778 * SN(512 + 20.2 * T);
        MM = 296.104608 + 477198.8491 * T + .009192 * T * T + 817e-6 * SN(512 + 20.2 * T);
        D = 350.737486 + 445267.1142 * T - .001436 * T * T + .002011 * SN(512 + 20.2 * T);
        Z = 259.183275 - 1934.142 * T + .002078 * T * T;
        F = 11.250889 + 483202.0251 * T - .003211 * T * T - .024691 * SN(Z);
        E = 1 - .002495 * T - 752e-8 * T * T;
        LUNK = L + 6.28875 * Math.sin(MM / G) + 1.274018 * Math.sin((2 * D - MM) / G) + .658309 * Math.sin(2 * D / G) + .213616 * Math.sin(2 * MM / G);
        LUNK -= .185596 * E * Math.sin(M / G);
        LUNK -= .114336 * Math.sin(2 * F / G) + .058793 * Math.sin((2 * D - 2 * MM) / G) + .057212 * E * Math.sin((2 * D - M - MM) / G);
        LUNK += .05332 * Math.sin((2 * D + MM) / G);
        LUNK += .045874 * E * Math.sin((2 * D - M) / G) + .041024 * E * Math.sin((MM - M) / G) - .034718 * Math.sin(D / G) - .030465 * E * Math.sin((M + MM) / G);
        LUNK += .015326 * Math.sin((2 * D - 2 * F) / G);
        LUNK -= .012528 * Math.sin((2 * F + MM) / G) - .01098 * Math.sin((2 * F - MM) / G) + .010674 * Math.sin((4 * D - MM) / G);
        LUNK += .010034 * Math.sin(3 * MM / G) + .008548 * Math.sin((4 * D - 2 * M) / G) - .00791 * E * Math.sin((M - MM + 2 * D) / G);
        LUNK -= .006783 * E * SN(2 * D + M) + .005162 * SN(MM - D) + .005 * SN(M + D) * E + .004049 * SN(MM - M + 2 * D) * E;
        LUNK += .003996 * SN(2 * MM + 2 * D) + .003862 * SN(4 * D) + .003665 * Math.sin(2 * D - 3 * MM);
        LUNK += .002695 * SN(2 * MM - M) * E + .002602 * SN(MM - 2 * F - 2 * D) + .002396 * SN(2 * D - M - 2 * MM) * E;
        LUNK -= .002349 * SN(MM + D) + .002249 * SN(2 * D - 2 * M) * E * E - .002125 * SN(2 * MM + M) * E;
        LUNK -= .002079 * SN(2 * M) * E * E + .002059 * SN(2 * D - MM - 2 * M) * E * E;
        LUNS1 = KRUG(LUNK + .005);
        return LUNS1;
    }
    function OKRUG(A) {
        var OKRUG1;
        OKRUG1 = KRUG(A) > 180 ? KRUG(A) - 360 : KRUG(A);
        return OKRUG1;
    }
    function NOL(T) {
        var TN, T1, NOL1;
        TN = KRUG(LUNS(T) - SOLS(T));
        T1 = T - TN / 12.2 / 36525;
        TN = OKRUG(LUNS(T1) - SOLS(T1));
        T1 -= TN / 12.2 / 36525;
        TN = OKRUG(LUNS(T1) - SOLS(T1));
        T1 -= TN / 12.2 / 36525;
        TN = OKRUG(LUNS(T1) - SOLS(T1));
        T1 -= TN / 12.2 / 36525;
        TN = OKRUG(LUNS(T1) - SOLS(T1));
        NOL1 = T1 - TN / 12.2 / 36525;
        return NOL1;
    }
    function LD(D, M, Y, H, MIN, BRG, BRM, LNG, LNM) {
        var T, TN, TV, TVN, LD1;
        T = TT(D, M, Y, H, MIN, 0);
        TV = T + Math.abs(T, BRG, BRM, LNG, LNM) / 1440 / 36525;
        if (T > TV) LD1 = 0; else {
            TN = NOL(T);
            TVN = TN + Math.abs(TN, BRG, BRM, LNG, LNM) / 1440 / 36525;
            LD1 = Math.round(35266 * (TV - TVN)) + 1;
        }
        return LD1;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "black",
        fullscreen: "true",
        exitOnClose: "true",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    exports.destroy = function() {};
    _.extend($, $.__views);
    null == Ti.App.Properties.getObject("isSoundOn") && Ti.App.Properties.setBool("isSoundOn", true);
    null == Ti.App.Properties.getString("SETTING_LANGUAGE") && Ti.App.Properties.setString("SETTING_LANGUAGE", Ti.Locale.currentLocale);
    var videoPrefix = "1242_2208";
    var videoSuffix = "";
    var badDays = [ 9, 19, 29 ];
    var moonDay = 0;
    var settings = null;
    var moonPlayer = null;
    var interval = null;
    var detailsBackground = null;
    var settingsButton = null;
    var dateView = null;
    var dateLabel = null;
    var dateTitleLabel = null;
    var dateDescriptionLabel = null;
    var pWidth;
    var pHeight;
    var isiOS = true;
    var resumePlayer = function() {
        calculateMoonDay();
        moonPlayer.play();
        resumeSound();
    };
    var resumeSound = function() {
        if (null == settings) {
            stopMusicFunction();
            clearInterval(interval);
            interval = setInterval(function() {
                playMoonSound();
            }, 5e3);
            playMoonSound();
        }
    };
    var stopMusicFunction = function() {
        clearInterval(interval);
        sound && sound.stop();
    };
    if (isiOS) {
        pWidth = Ti.Platform.displayCaps.platformWidth;
        pHeight = Ti.Platform.displayCaps.platformHeight;
        switch (pWidth) {
          case 320:
            videoPrefix = 480 == pHeight ? "640_960" : "750_1334";
            break;

          case 375:
            videoPrefix = "750_1334";
            break;

          case 768:
            videoPrefix = "640_960";
            break;

          case 414:
            videoPrefix = "1242_2208";
        }
    } else videoPrefix = "640_960";
    if (isiOS) {
        showMoon();
        dateView.opacity = 0;
        settingsButton.opacity = 0;
        moonPlayer.opacity = 0;
        showIntro();
    }
    Ti.API.info("videoPrefix: " + videoPrefix);
    Ti.API.info("width: " + pWidth);
    Ti.API.info("height: " + pHeight);
    $.index.open();
    $.index.addEventListener("open", function() {
        showMoon();
        dateView.opacity = 0;
        settingsButton.opacity = 0;
        moonPlayer.opacity = 0;
        showIntro();
        if (false == isiOS) {
            $.index.activity.addEventListener("resume", resumePlayer);
            $.index.activity.addEventListener("pause", stopMusicFunction);
            $.index.activity.addEventListener("stop", function() {
                $.index.activity.removeEventListener("resume", resumePlayer);
            });
        } else {
            Ti.App.addEventListener("resume", resumePlayer);
            Ti.App.addEventListener("pause", stopMusicFunction);
        }
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;