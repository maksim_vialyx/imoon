function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function close() {
        playSound("button_2");
        $.settings.close();
    }
    function openReviews() {
        playSound("button_3");
        Ti.Platform.openURL("https://www.weblancer.net/users/modisoftpro/portfolio/");
    }
    function changeSound() {
        playSound("button_4");
        var isSoundOn = Ti.App.Properties.getBool("isSoundOn");
        Ti.App.Properties.setBool("isSoundOn", !isSoundOn);
        updateSoundBtn();
    }
    function changeLanguage() {
        playSound("button_5");
        var opts = {
            cancel: 3,
            options: [ "Русский", "English", "Swedish", L("close") ],
            destructive: -1,
            title: ""
        };
        var dialog = Ti.UI.createOptionDialog(opts);
        dialog.addEventListener("click", function(event) {
            if (-1 != event.index) {
                var locale = "en-EN";
                0 == event.index ? locale = "ru-RU" : 1 == event.index ? locale = "en-EN" : 2 == event.index && (locale = "sv-SE");
                Ti.App.languageXML = null;
                Ti.App.Properties.setString("SETTING_LANGUAGE", locale);
                updateLanguage();
            }
        });
        dialog.show();
    }
    function updateLanguage() {
        $.headerLabel.text = L("settings_title");
        $.descriptionTextView.value = L("settings_content");
        $.langLabel.text = L("settings_language");
        $.soundLabel.text = L("settings_sound");
        $.reviewsLabel.text = L("settings_reviews");
    }
    function updateSoundBtn() {
        var isSoundOn = Ti.App.Properties.getBool("isSoundOn");
        $.soundBtn.backgroundImage = isSoundOn ? "/images/sound_on.png" : "/images/sound_off.png";
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "settings";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.settings = Ti.UI.createWindow({
        fullscreen: "true",
        id: "settings"
    });
    $.__views.settings && $.addTopLevelView($.__views.settings);
    $.__views.__alloyId0 = Ti.UI.createImageView({
        image: "/images/background_settings.png",
        top: "0",
        left: "0",
        width: "100%",
        height: "100%",
        id: "__alloyId0"
    });
    $.__views.settings.add($.__views.__alloyId0);
    $.__views.overlayView = Ti.UI.createView({
        id: "overlayView",
        top: "15%",
        left: "5%",
        right: "5%",
        bottom: "10%"
    });
    $.__views.settings.add($.__views.overlayView);
    $.__views.overlayImage = Ti.UI.createImageView({
        id: "overlayImage",
        image: "/images/overlay_settings.png",
        top: "0",
        bottom: "0"
    });
    $.__views.overlayView.add($.__views.overlayImage);
    $.__views.headerLabel = Ti.UI.createLabel({
        text: L("settings_title", "settings_title"),
        textAlign: "center",
        color: "white",
        font: {
            fontSize: "32sp"
        },
        id: "headerLabel",
        top: "15sp",
        left: "5%",
        right: "5%",
        height: "40sp"
    });
    $.__views.overlayView.add($.__views.headerLabel);
    $.__views.descriptionTextView = Ti.UI.createTextArea({
        color: "white",
        value: L("settings_content", "settings_content"),
        font: {
            fontSize: "20sp"
        },
        id: "descriptionTextView",
        top: "50sp",
        left: "5%",
        right: "5%",
        bottom: "30%",
        editable: "false",
        backgroundColor: "transparent"
    });
    $.__views.overlayView.add($.__views.descriptionTextView);
    $.__views.languageBtn = Ti.UI.createButton({
        id: "languageBtn",
        backgroundImage: "/images/language_icon.png",
        bottom: "10%",
        left: "10%",
        width: "50sp",
        height: "50sp"
    });
    $.__views.overlayView.add($.__views.languageBtn);
    changeLanguage ? $.addListener($.__views.languageBtn, "click", changeLanguage) : __defers["$.__views.languageBtn!click!changeLanguage"] = true;
    $.__views.soundBtn = Ti.UI.createButton({
        id: "soundBtn",
        backgroundImage: "/images/sound_on.png",
        bottom: "10%",
        width: "50sp",
        height: "50sp"
    });
    $.__views.overlayView.add($.__views.soundBtn);
    changeSound ? $.addListener($.__views.soundBtn, "click", changeSound) : __defers["$.__views.soundBtn!click!changeSound"] = true;
    $.__views.reviewsBtn = Ti.UI.createButton({
        id: "reviewsBtn",
        backgroundImage: "/images/reviews_icon.png",
        bottom: "10%",
        right: "10%",
        width: "50sp",
        height: "50sp"
    });
    $.__views.overlayView.add($.__views.reviewsBtn);
    openReviews ? $.addListener($.__views.reviewsBtn, "click", openReviews) : __defers["$.__views.reviewsBtn!click!openReviews"] = true;
    $.__views.langLabel = Ti.UI.createLabel({
        color: "white",
        text: L("settings_language", "settings_language"),
        textAlign: "center",
        font: {
            fontSize: "8sp"
        },
        id: "langLabel",
        bottom: "6%",
        left: "10%",
        width: "50sp",
        height: "4%"
    });
    $.__views.overlayView.add($.__views.langLabel);
    $.__views.soundLabel = Ti.UI.createLabel({
        color: "white",
        text: L("settings_sound", "settings_sound"),
        textAlign: "center",
        font: {
            fontSize: "8sp"
        },
        id: "soundLabel",
        bottom: "6%",
        width: "50sp",
        height: "4%"
    });
    $.__views.overlayView.add($.__views.soundLabel);
    $.__views.reviewsLabel = Ti.UI.createLabel({
        color: "white",
        text: L("settings_reviews", "settings_reviews"),
        textAlign: "center",
        font: {
            fontSize: "8sp"
        },
        id: "reviewsLabel",
        bottom: "6%",
        right: "10%",
        width: "50sp",
        height: "4%"
    });
    $.__views.overlayView.add($.__views.reviewsLabel);
    $.__views.__alloyId1 = Ti.UI.createButton({
        backgroundImage: "/images/close_settings.png",
        right: "4.5%",
        top: "14.5%",
        width: "30sp",
        height: "30sp",
        id: "__alloyId1"
    });
    $.__views.settings.add($.__views.__alloyId1);
    close ? $.addListener($.__views.__alloyId1, "click", close) : __defers["$.__views.__alloyId1!click!close"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.overlayImage.backgoundImage = $.overlayImage.image;
    updateLanguage();
    updateSoundBtn();
    __defers["$.__views.languageBtn!click!changeLanguage"] && $.addListener($.__views.languageBtn, "click", changeLanguage);
    __defers["$.__views.soundBtn!click!changeSound"] && $.addListener($.__views.soundBtn, "click", changeSound);
    __defers["$.__views.reviewsBtn!click!openReviews"] && $.addListener($.__views.reviewsBtn, "click", openReviews);
    __defers["$.__views.__alloyId1!click!close"] && $.addListener($.__views.__alloyId1, "click", close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;