function playSound(name) {
    if (false == Ti.App.Properties.getBool("isSoundOn")) return;
    sound = Titanium.Media.createSound({
        url: "/sounds/" + name + ".mp3"
    });
    sound.play();
}

function L(text) {
    if (void 0 === Ti.App.languageXML || null === Ti.App.languageXML) {
        var langFile = Ti.App.Properties.getString("SETTING_LANGUAGE");
        var file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, "languages/" + langFile + "/strings.xml");
        if (!file.exists()) {
            var langFile = "en-EN";
            file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, "languages/" + langFile + "/strings.xml");
        }
        var xmltext = file.read().text;
        var xmldata = Titanium.XML.parseString(xmltext);
        Ti.App.languageXML = xmldata;
    }
    var xpath = "/resources/string[@name='" + text + "']/text()";
    var result = Ti.App.languageXML.evaluate(xpath).item(0);
    return result ? result.text : text;
}

var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

var sound = null;

Alloy.createController("index");